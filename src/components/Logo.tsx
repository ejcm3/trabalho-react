import { useFonts, ArchitectsDaughter_400Regular } from '@expo-google-fonts/architects-daughter';
import {BackgroundLogo, Titulo} from './style';


const Logo=()=>{
    let [fontsLoaded] = useFonts({
        ArchitectsDaughter_400Regular,
      });
    return(
        <BackgroundLogo>
            <Titulo>Reus-e</Titulo>
        </BackgroundLogo>
    )
}
export default Logo;