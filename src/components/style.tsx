import styled from "styled-components/native";
import { style } from "../global/global";

export const BackgroundLogo= styled.View`
        background-color: ${style.colors.mainRed};
        width: 360px;
        height: 126px;
        align-items: center;
        justify-content: center;
        `
export const Titulo = styled.Text`
        position: absolute;
        font-family: ${style.fonts.ad400r};
        font-size: 70px;
        color: white;
        `