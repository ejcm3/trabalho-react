import { TextInput, View, StyleSheet } from "react-native";

const Input=({param})=>{
    return(
        <View>
            <TextInput secureTextEntry={param} style={styles.input}></TextInput>
        </View>
    )
}

const styles=StyleSheet.create({
    input:{
        width: 330,
        height: 40,
        marginTop:15,
        marginLeft:15,
        fontSize:25,
        color:"white",
        padding: 5,
    }
})
export default Input;