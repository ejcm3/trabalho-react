import React from "react";
import Logo from "../../components/Logo";
import Input from "../../components/Input";
import {BackgroundLogin, ButtonLogin, Email, EsqueciSenha, Password, SemCadastro, TextLogin} from "./style";

const Login = ()=>{
    return(
        <BackgroundLogin>
            <Logo></Logo>
            <Email>e-mail:</Email>
            <Input param={false}></Input>
            <Password>senha:</Password>
            <Input param={true}></Input>
            <EsqueciSenha>Esqueci a senha</EsqueciSenha>
            <SemCadastro>Não sou cadastrado</SemCadastro>
            <ButtonLogin>
                <TextLogin>LOGIN</TextLogin>
            </ButtonLogin>
        </BackgroundLogin>
    )
}


export default Login;