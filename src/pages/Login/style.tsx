import styled from "styled-components/native";
import { style } from "../../global/global";

export const BackgroundLogin = styled.View`
    background-color: ${style.colors.mainGray};
    height: 800px;
    width: 360px;
    align-self: center;
    `

export const Email = styled.Text`
    font-size: 40;
    font-family: ${style.fonts.as};
    margin-top: 64;
    margin-left: 20;
    color: white;
    `
export const Password = styled.Text`
    font-size: 40;
    font-family: ${style.fonts.as};
    margin-top: 20;
    margin-left: 20;
    color: white;
    `
export const EsqueciSenha= styled.Text`
    font-size:25;
    font-family: ${style.fonts.as};
    color: blue;
    margin-top: 20;
    margin-left: 20;
    text-decoration-line: underline;
`
export const SemCadastro= styled.Text`
    font-size:25;
    font-family: ${style.fonts.as};
    text-align: right;
    margin-top: 64;
    margin-right: 20;
    color: white;
    text-decoration-line: underline;
`
export const ButtonLogin = styled.TouchableOpacity`
    background-color: ${style.colors.mainRed};
    width: 200;
    height: 70;
    left: 80;
    margin-top: 110;
    align-items:center;
    justify-content:center;
`
export const TextLogin= styled.Text`
    font-family: ${style.fonts.as};
    font-size: 40;
    color: white;
`