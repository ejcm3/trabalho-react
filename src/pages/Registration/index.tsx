import React from "react";
import Logo from "../../components/Logo";
import Input from "../../components/InputRegistration";
import {BackgroundCadastro, ButtonRegistration, Label, TextBelow, UsingTerms, Terms, TextRegistration} from "./style";

const Registration = ()=>{
    return(
        <BackgroundCadastro>
            <Logo></Logo>
            <Label>nome:</Label>
            <Input param={false}></Input>
            <Label>data de nascimento:</Label>
            <Input param={false}></Input>
            <Label>e-mail:</Label>
            <Input param={false}></Input>
            <Label>senha:</Label>
            <Input param={true}></Input>
            <Label>confirme senha:</Label>
            <Input param={true}></Input>
            <UsingTerms>
                <input type="checkbox"/>
                <TextBelow>Concordo com os </TextBelow>
                <Terms>Termos de Uso</Terms>
            </UsingTerms>
            <UsingTerms>
                <input type="checkbox"/>
                <TextBelow>Quero receber e-mails promocionais</TextBelow>
            </UsingTerms>
            <ButtonRegistration>
                <TextRegistration>CADASTRAR</TextRegistration>
            </ButtonRegistration>
        </BackgroundCadastro>
    )
}


export default Registration;