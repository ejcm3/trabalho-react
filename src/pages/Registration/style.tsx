import styled from "styled-components/native";
import { style } from "../../global/global";

export const BackgroundCadastro = styled.View`
    background-color: ${style.colors.mainGray};
    height: 800px;
    width: 360px;
    align-self: center;
    `
export const Label = styled.Text`
    font-size: 20;
    font-family: ${style.fonts.as};
    margin-top: 20;
    margin-left: 20;
    color: white;
    `
export const UsingTerms = styled.View`
    flex-direction:row;
    margin-left: 15;
    align-itens: center;
    margin-top: 20;
    `
export const TextBelow= styled.Text`
    font-size:18;
    font-family: ${style.fonts.as};
    color: white;
    margin-left: 5;
    `
export const Terms = styled.Text`
    font-size:18;
    font-family: ${style.fonts.as};
    color: white;
    text-decoration-line: underline;
`
export const ButtonRegistration = styled.TouchableOpacity`
    background-color: ${style.colors.mainRed};
    width: 200;
    height: 70;
    left: 80;
    margin-top: 25;
    align-items:center;
    justify-content:center;
`
export const TextRegistration= styled.Text`
    font-family: ${style.fonts.as};
    font-size: 25;
    color: white;
`