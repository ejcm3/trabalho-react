export const style={
    colors:{
        mainRed: '#A40000',
        mainGray: '#251F1F',
    },
    fonts:{
        ad400r:"ArchitectsDaughter_400Regular",
        as: "Arbutus Slab",
    }
}